var modalApp = new Vue({
  el: '#modal-app',
  data: {
    modalShown: false,
    title: '',
    body: '',
    action: () => {}
   },
  methods: {
    show (title, body, action) {
      this.title = title;
      this.body = body;
      this.action = action;
      this.modalShown = true;
    },
    close () {
      this.modalShown = false;
    }
  },
  beforeMount() {
  }
})