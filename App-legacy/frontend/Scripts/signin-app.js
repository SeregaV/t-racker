var signinApp = new Vue({
  el: '#signin-app',
  data: {
    labelSignin: 'Sign In',
    labelUname: 'Email',
    labelPass: 'Password',
    labelSubmit: 'Sign In',
    idUname: 'funame',
    idPass: 'fpass',
    login: '',
    password: '',
    initialitems: [
      'Item one',
      '2 Two',
      'Somebody says...',
      'Lorem ipsum',
      'And more'
    ],
    items: [],
    searchVal: []
  },
  methods: {
    signin () {
      socketApp.io.emit('action', { method: 'signin', login: this.login, password: this.password })
    },
    inputIt: function (e) {
      this.items = this.initialitems.filter(item => item.toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1)
      this.searchVal = e.target.value
    },
    onselected (sel) {
      this.searchVal = sel
    },
    mailchange (e) {
      this.login = e.target.value;
    },
    passchange (e) {
      this.password = e.target.value;
    }
  },
  mounted(){
    this.items = [...this.initialitems]
  }
})