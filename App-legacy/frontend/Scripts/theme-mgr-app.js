const themes = {
  light: "./Styles/light-shift.css",
  dark: "./Styles/dark-shift.css",
  cyborg: "https://bootswatch.com/4/cyborg/bootstrap.css",
  flatly: "https://bootswatch.com/4/flatly/bootstrap.css"
};

var themeMgr = new Vue({
  el: '#theme-mgr',
  data: {
    themeHelper: new ThemeHelper(),
  },
  methods: {
    load: function () {
        let added = Object.keys(themes).map(n => this.themeHelper.add(n, themes[n]));
        let defaultTheme = 'light'
        
        if(localStorage.getItem('theme'))
            defaultTheme = localStorage.getItem('theme')

        Promise.all(added).then(sheets => {
            console.log(`${sheets.length} themes loaded`);
            this.themeHelper.theme = defaultTheme;
        });
    },
    toggle: function() {
        let newTheme = this.themeHelper.theme === "dark" ? "light" : "dark";
        this.themeHelper.theme = newTheme
        localStorage.setItem('theme', newTheme)
    }
  },
  beforeMount() {
      this.load();
  }
})