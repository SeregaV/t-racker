Vue.component('cmp', {
    template: '<v-button :onclick="melog">Click Me</v-button>',
    methods: {
        melog: function() {
            modalApp.show("Dummy", "Dummy content", () => {console.log("Damn!");modalApp.close()})
        }
    }, 
})

var tabapp = new Vue({
    el: '#tab-app',
    data: function () { 
        let randomNumber = Math.random() * 1000
        return {
            tabs: [
                {
                    title: 'Tab',
                    content: `Tab 1 content`
                },
                {
                    title: 'Computed',
                    content: `<strong> Random: </strong><i>{{ ${randomNumber} }}</i>`
                },
                {
                    title: 'Component',
                    component: 'cmp'
                }
            ]
        }
    },
    beforeMount() {}
  })