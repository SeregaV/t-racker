var app = new Vue({
    el: '#app',
    data: {
      btnRun: 'Run',
      tracking: false,
      tracked: '00:00:00',
      started: null,
      activity: null,
      wholeProjects: [
        'LMV',
        'Trusty-Care',
        'VXP',
        'symplete',
        'GCF',
        'InSight'
      ],
      wholeTasks: [
        'Sample task',
        'Sample Task 2',
        'Another task',
        'Debug task',
        'Design task'
      ],
      tasks: [],
      projects: [],
      project: '',
      task: '',
      overall: '00:00:00',
      overallInt: 0
    },
    methods: {
        load () {
            this.projects = [ ...this.wholeProjects ];
            this.tasks = [ ...this.wholeTasks ];
            console.log(localStorage.getItem('user'))
            socketApp.io.on('projects', function(projs) {
                app.wholeProjects = projs;
                app.update();
            })
            socketApp.io.on('gitlab_auth_next', (url) => window.open(url, 'GitLab'))
            socketApp.io.emit('getprojects');
            this.getoverall();
        },
        notGitLabAuth () {
            console.log('===============================================')
            console.log(JSON.parse(localStorage.getItem("user")).gitlab);
            console.log('===============================================')
            return !(JSON.parse(localStorage.getItem("user")).gitlab && JSON.parse(localStorage.getItem("user")).gitlab.id);
        },
        update(){
            this.project = '';
            this.projects = [ ...this.wholeProjects ];
        },
        getoverall(){
            socketApp.io.emit('action', { method: 'getoverall' }, ret => {
                app.overallInt = +ret[1];
                app.overall = moment.utc(moment.duration(app.overallInt, 's').as('ms')).format('HH:mm:ss');
            })
        },
        toggleTimer: () => {
            app.tracking = !app.tracking;
            app.btnRun = app.tracking ? "Stop" : "Run";
            if(app.tracking){
                socketApp.io.emit('action', { method: 'starttrack' }, (ret) => {
                    let activity = ret[1];
                    app.start = activity.start;
                    if(app.timer)
                    {
                        clearInterval(app.timer)
                    }
                    app.timer = setInterval(app.tick, 1000);
                });
            }
            else
            {
                socketApp.io.emit('action', {method: 'stoptrack'}, (ret) => {
                    if(ret[0] != 'stopped')
                    {   
                        modalApp.show('Error', ret[0]);
                        return;
                    }
                    if(app.timer)
                    {
                        clearInterval(app.timer)
                        app.timer = null
                    }
                })
            }
            app.getoverall();
        },
        tick: () => {
            socketApp.io.emit('action', { method: 'track' }, (ret) => {
                let activity = ret[1];
                app.activity = activity;
                app.tracked = moment().startOf('day').seconds(activity.tracked).format('HH:mm:ss'); //moment.utc(moment.utc(activity.start).diff(moment.utc(), "s")).format('HH:mm:ss');
                ++app.overallInt;
                app.overall = moment.utc(moment.duration(app.overallInt, 's').as('ms')).format('HH:mm:ss');
            });
            // app.tracked = moment.utc(moment().diff(app.started, "ms")).format('HH:mm:ss');
        },
        searchProject (e) {
            this.projects = this.wholeProjects.filter(item => item.toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1)
            this.project = e.target.value
        },
        searchTask (e) {
            this.tasks = this.wholeTasks.filter(item => item.toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1)
            this.task = e.target.value
        },
        onselectproj (proj) {
            this.project = proj;
            socketApp.io.emit('get-tasks', proj);
        },
        onselecttask (task) {
            this.task = task;
        },
        opensettings () {
            settingsApp.settings_visible = true;
            settingsApp.load()
        },
        runDisabled () {
            // console.log(settingsApp.canStartIfNoTask && this.task)
            // console.log(settingsApp.canStartIfNoTask)
            // const can = localStorage.getItem("canStartIfNoTask");
            const disabled = ((!settingsApp.canStartIfNoTask) && !(this.task.length > 0));
            return disabled;
        }
    },
    beforeMount() {
        this.load();
    }
  })