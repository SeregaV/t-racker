var settingsApp = new Vue({
    el: '#settings-app',
    data: {
      gitlab_authed: false,
      canStartIfNoTask: false,
      settings_visible: false
    },
    methods: {
        load () {
            this.canStartIfNoTask = JSON.parse(localStorage.getItem("canStartIfNoTask"));
        },
        notGitLabAuth () {
            return !(JSON.parse(localStorage.getItem("user")).gitlab && JSON.parse(localStorage.getItem("user")).gitlab.id);
        },
        logout () {
            routerApp.logout();
        },
        gitlabauth () {
            socketApp.io.emit('action', { method: 'auth_gitlab' } )
        },
        closesettings(){
            this.settings_visible = false;
        }
    },
    watch: {
        canStartIfNoTask: function(val) {
            localStorage.setItem("canStartIfNoTask", this.canStartIfNoTask);
        }
    },
    beforeMount() {
        this.load();
    }
  })