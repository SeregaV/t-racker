var socketApp = new Vue({
  el: '#socket-app',
  data: function() {
    let _io = io('http://localhost:3000')
    console.log(_io)
    _io.on('error', (reason) => {
        modalApp.show('Socket Error', reason)
    })
    _io.on('connect_error', function(err) {
        modalApp.show('Socket Error', err)
    });
    console.log('socket ready')
    return {
        io: _io 
    }
},
  methods: {
      load: () => {
        
      },
  },
  created() {
     this.load()
  }
})