const routes = {
  '/': 'index',
  '/signin': 'signin',
  '/timer': 'timer'
}

var routerApp = new Vue({
  el: '#router-app',
  data: {
    currentRoute: localStorage.getItem('page'),
  },
  methods: {
    goto: function (to) {
      localStorage.setItem('page', to)
      socketApp.io.emit('load', to)
    },
    logout: function () {
      socketApp.io.emit('action', { method: 'logout' })
    },
    logoutEverywhere: function() {
      socketApp.io.emit('action', {method: 'logouteverywhere'})
    }
  },
  beforeMount () {
    this.currentRoute = localStorage.getItem('page');
    console.log(this.currentRoute)
    let user = JSON.parse(localStorage.getItem('user'));

    socketApp.io.emit('log', sessionStorage.getItem('loaded'));
    if((!localStorage.getItem('isAuthed') || !localStorage.getItem('user') || !user.access_token) && this.currentRoute !== 'signin')
    {
      sessionStorage.setItem('loaded', true);
      this.goto('signin');
      socketApp.io.emit('log', sessionStorage.getItem('loaded'));
    }
    else if((localStorage.getItem('isAuthed') && localStorage.getItem('user') && user.access_token && !sessionStorage.getItem('loaded')) && this.currentRoute !== 'timer')
    {
      sessionStorage.setItem('loaded', true);
      socketApp.io.emit('action', { method: 'signin', login: user.email, token: user.access_token, ref_token: user.refresh_token }, (result) => {
        socketApp.io.emit('log', `SIGNIN RESULT: ${JSON.stringify(result)}`)
        if(result[0] == 'signin-ok')
        {
          this.goto('timer')
        }
        else
        {
          this.goto('signin')
        }
      })
    }
    else if(localStorage.getItem('isAuthed') && this.currentRoute == 'timer')
    {
      socketApp.io.emit('action', { method: 'getuser'})
    }
  },
  mounted () {
    socketApp.io.on('loggedout', function () {
      console.log('logging out...');
      localStorage.removeItem('isAuthed');
      localStorage.removeItem('user');
      // process.loaded = false;
      routerApp.goto('signin');
    })
    socketApp.io.on("signin-ok", (user) => {
      console.log('SignIn Ok')
      localStorage.setItem('isAuthed', true)
      localStorage.setItem('user', JSON.stringify(user))
      routerApp.goto("timer")
    })
    socketApp.io.on("user", (user) => {
      console.log('User coming in')
      localStorage.setItem('isAuthed', true)
      localStorage.setItem('user', JSON.stringify(user))
      if(this.currentRoute == 'timer'){
        settingsApp.gitlab_authed = true&&user.gitlab;
      }
    })
    socketApp.io.on("signin-fail", (reason) => {
      console.error(reason)
      modalApp.show('Error', reason)
    })
  }
  // computed: {
  //   ViewComponent () {
  //     return routes[this.currentRoute] || 'NotFound'
  //   }
  // },
  // render (h) { return h(this.ViewComponent) }
})