Vue.component("render-string", {
  props: {
    string: {
      required: true,
      type: String
    }
  },
  render(h) {
    const render = {
      template: "<div>" + this.string + "</div>",
      methods: {
        markComplete() {
          console.log('the method called')
        }
      }
    }
    return h(render)
  }
})