Vue.component('v-button', {
  props: ['label', 'onclick', 'disabled'],
  template: `
  <button v-on:click="onclick" :disabled="disabled">
    <a href="#">
      <slot></slot> 
      <span v-if="!disabled" class="shift">›</span>
    </a>
    <div class="mask"></div>
  </button>`
})