Vue.component('card', {
  props: ['title', 'header-image', 'content', 'footer'],
  template: `
  <div class="card">
    <div class="card-header">
      <h2>{{title}}</h2>
    </div>
    <div class="card-content">
    
    </div>
    <div class="card-footer">
    
    </div>
  </div>`
})