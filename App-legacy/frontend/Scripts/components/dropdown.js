Vue.component('dropdown', {
  props: [ 'onselect', 'label', 'options', 'value', 'oninput' ],
  template: `<div class="md-select">
  <input id="ind" type="text" placeholder=" " :value="value" v-on:input="oninput">
  <label for="ind">{{label}}</label>
  <div class="indicator"></div>
  <ul>
    <li v-for="option in options" class="option" v-on:click="onselect(option||'')">{{option}}</li>
  </ul>
</div>`
})