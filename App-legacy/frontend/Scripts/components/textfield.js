Vue.component('textfield', {
  props: ['id', 'label', 'type', 'oninput'],
  template: `
  <div class="md-textfield">
    <input type="text" class="md-textfield-input" :type="type" :id="id" :name="id" placeholder=" " v-on:input="oninput" autocomplete="off">
    <label :for="id">{{label}}</label>
    <div class="indicator"></div>
  </div>`
})