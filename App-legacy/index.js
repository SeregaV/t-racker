const { app, BrowserWindow } = require('electron')
// const methods = require('./backend/index').default
const io = require('socket.io')(3000, {
  path: '/',
  serveClient: false,
  // below are engine.IO options
  pingInterval: 10000,
  pingTimeout: 5000,
  cookie: false
})

const server = require('socket.io-client')('https://t-racker.herokuapp.com');//http://localhost:3366')
let g_socket;

// Храните глобальную ссылку на объект окна, если вы этого не сделаете, окно будет
// автоматически закрываться, когда объект JavaScript собирает мусор.
let win;

function createWindow () {
  // Создаём окно браузера.
  win = new BrowserWindow({
    // width: 1200,
    // height: 462,
    width: 336,
    height: 462,
    transparent:true,
    frame: false,
    webPreferences: {
      nodeIntegration: true,
      experimentalFeatures: true
    }
  })

  win.webContents.on('new-window', (event, url, frameName, disposition, options, additionalFeatures) => {
    if (frameName === 'GitLab') {
      // открыть окно как модальное
      event.preventDefault()
      let modal =  new BrowserWindow({
        ...options,
        modal: true,
        parent: win,
        frame: true,
        transparent: false
      });
      
      modal.once('ready-to-show', () => modal.show())
      modal.setMenu(null);
      modal.loadURL(url);
      // console.log(url)
      modal.on('closed', (e) => {
        if(g_socket)
        {
          server.emit('action', { method: 'getuser' }, function (response) {
            console.log('Update user', response);
      
            g_socket.emit(...response);
          });
          // socket.emit(...methods[payload.method](payload));
        }
      });
      
      // if (!options.webContents) {
      //   modal.loadURL(url) // existing webContents will be navigated automatically
      // }
      event.newGuest = modal
    }
  })

  // win.webContents.on('new-window', (event, url, frameName, disposition, options) => {
  //   event.preventDefault()
  //   const modalWin = new BrowserWindow({
  //     webContents: options.webContents, // use existing webContents if provided
  //     show: false
  //   })
  //   modalWin.once('ready-to-show', () => modalWin.show())
  //   if (!options.webContents) {
  //     modalWin.loadURL(url) // existing webContents will be navigated automatically
  //   }
  //   event.newGuest = modalWin
  // })

  // win.setMenu(null);
  win.setResizable(false);
  // and load the index.html of the app.
  win.loadFile('frontend/index.html')

  // Отображаем средства разработчика.
  // win.webContents.openDevTools()

  // Будет вызвано, когда окно будет закрыто.
  win.on('closed', () => {
    // Разбирает объект окна, обычно вы можете хранить окна     
    // в массиве, если ваше приложение поддерживает несколько окон в это время,
    // тогда вы должны удалить соответствующий элемент.
    win = null
  })
}

io.on('connection', function(socket){
  console.log('app connected');
  g_socket = socket;
  // sockets.push(socket)

  socket.on('load', (page) => {
    win.loadFile(`frontend/${page}.html`)
  })

  socket.on('action', (payload, cb) => {
    payload = { ...payload };
    console.log(payload);
    server.emit('action', payload, function (response) {
      console.log(response);
      
      // if(response[0] == 'gitlab_auth_next')
      //   window.open(response[1]);

      socket.emit(...response);
      if(cb)
        cb(response);
    });
    // socket.emit(...methods[payload.method](payload));
  })

  socket.on('log', (args) => {
    console.log('LOG:', args)
  })

  // socket.on('logout', () => {
  //   socket.emit('loggedout')
  // })

  socket.on('disconnect', function(){
    console.log('app disconnected');
  });
});

// Этот метод будет вызываться, когда Electron закончит 
// инициализацию и готов к созданию окон браузера.
// Некоторые API могут использоваться только после возникновения этого события.
app.on('ready', createWindow)

// Выходим, когда все окна будут закрыты.
app.on('window-all-closed', () => {
  // Для приложений и строки меню в macOS является обычным делом оставаться
  // активными до тех пор, пока пользователь не выйдет окончательно используя Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
   // На MacOS обычно пересоздают окно в приложении,
   // после того, как на иконку в доке нажали и других открытых окон нету.
  if (win === null) {
    createWindow()
  }
})

// В этом файле вы можете включить код другого основного процесса 
// вашего приложения. Можно также поместить их в отдельные файлы и применить к ним require.