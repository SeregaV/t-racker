const signin = require('./api/signin').default;
const logout = require('./api/logout').default;

exports.default = {
  signin,
  logout
}