const { app, BrowserWindow, Tray, Menu } = require('electron')
const notifier = require('node-notifier');
const path = require('path')
const Config = require('node-json-config')

var conf = new Config(path.join(__dirname, "settings.json"));

const io = require('socket.io')(3000, {
  path: '/',
  serveClient: false,
  // below are engine.IO options
  pingInterval: 10000,
  pingTimeout: 5000,
  cookie: false
})

// process.env.DEV = true;
const apiURL = process.env.DEV ? 'http://localhost' : 'https://t-racker.herokuapp.com'; 
console.log(apiURL);
const server = require('socket.io-client')(apiURL);//http://localhost:3366')
let g_socket;

// Храните глобальную ссылку на объект окна, если вы этого не сделаете, окно будет
// автоматически закрываться, когда объект JavaScript собирает мусор.
let win, tray;

function toggleWindow () {
  if(!win)
    return

  if(win.isVisible())
    win.hide()
  else
    showWindow()
}

function quit() {
  app.quitting = true;
  app.quit()
}

const showWindow = () => {
  const position = getWindowPosition()
  win.setPosition(position.x, position.y, false)
  win.show()
  win.focus()
}

const getWindowPosition = () => {
  const windowBounds = win.getBounds()
  const trayBounds = tray.getBounds()

  // Center window horizontally below the tray icon
  const x = Math.round(trayBounds.x + (trayBounds.width / 2) - (windowBounds.width / 2))

  // Position window 4 pixels vertically below the tray icon
  const y = Math.round(trayBounds.y + trayBounds.height + 4)

  return {x: windowBounds.x, y: windowBounds.y}
}


function configureWindow () {
  if(!win)
    return
  // console.log('Configure...')
  const alwaysOnTop = conf.get('alwaysOnTop')
  if(alwaysOnTop)
  {
    win.setAlwaysOnTop(true, "floating");
    win.setVisibleOnAllWorkspaces(true); // put the window on all screens
    win.focus(); // focus the window up front on the active screen
    showWindow()
  }
  else
  {
    win.setAlwaysOnTop(false);
    win.setVisibleOnAllWorkspaces(false); // put the window on all screens
    win.focus(); // focus the window up front on the active screen
    showWindow()
  }
  console.log(win.isAlwaysOnTop(), win.isVisibleOnAllWorkspaces())
  // console.log('alwaysOnTop', alwaysOnTop)
  
}

function createWindow () {
  win = new BrowserWindow({
    // width: 1200,
    // height: 462,
    width: 339,
    height: 462,
    transparent: true,
    frame: false,
    hasShadow: false,
    fullscreenable: false,
    resizable: false,
    show: false,
    webPreferences: {
      nodeIntegration: true,
      experimentalFeatures: true
    }
  })

  if(app.dock)
    app.dock.hide();
  else
    win.setMenu(null);

  tray = new Tray(path.join(__dirname, 'src/Resources/sub-logo.png'))
  tray.setToolTip('t-RAcker')
  
  const contextMenu = Menu.buildFromTemplate([
    { label: 'Toggle window', click: toggleWindow },
    { type: "separator" },
    { label: 'Quit', click: quit }
  ])

  tray.setContextMenu(contextMenu)
  tray.on('right-click', () => toggleWindow())

  win.webContents.on('new-window', (event, url, frameName, disposition, options, additionalFeatures) => {
    if (frameName === 'GitLab') {
      event.preventDefault()
      let modal =  new BrowserWindow({
        ...options,
        modal: true,
        parent: win,
        frame: true,
        transparent: false
      });
      
      modal.once('ready-to-show', () => modal.show())
      modal.setMenu(null);
      modal.loadURL(url);
      modal.on('closed', (e) => {
        if(g_socket)
        {
          server.emit('action', { method: 'getuser' }, function (response) {
            g_socket.emit(...response);
          });
        }
      });
      
      event.newGuest = modal
    }
  })

  configureWindow()

  // and load the index.html of the app.
  win.loadFile('./src/index.html')


  win.on('close', (event) => {
    if (!app.quitting) {
      event.preventDefault()
      win.hide()
    }
  })
  
  // Будет вызвано, когда окно будет закрыто.
  win.on('closed', () => {
    // Разбирает объект окна, обычно вы можете хранить окна     
    // в массиве, если ваше приложение поддерживает несколько окон в это время,
    // тогда вы должны удалить соответствующий элемент.
    win = null
  })

  win.on('ready-to-show', showWindow)
}

io.on('connection', function(socket){
  console.log('app connected');
  g_socket = socket;

  socket.on('action', (payload, cb) => {
    payload = { ...payload };
    
    server.emit('action', payload, function (response) {
      socket.emit(...response);
      if(cb)
        cb(response);
    });
  })

  socket.on('log', (args) => {
    console.log('LOG:', args)
  })

  socket.on('setv', ({param, value}) => {
    // console.log('set', param, value)
    conf.put(param, value)
    conf.save()

    configureWindow()
  })

  socket.on('getv', (param, cb) => {
    // console.log('get')
    const confParam = conf.get(param)
    // console.log(param, confParam)
    if(cb)
      cb(confParam)
  })

  socket.on('notify', ({title, body}) => {
    notifier.notify({
      title: title,
      message: body,
      icon: path.join(__dirname, 'src/Resources/sub-logo.png'),
      wait: false,
    })
  })

  socket.on('disconnect', function(){
    console.log('app disconnected');
  });
});

// Этот метод будет вызываться, когда Electron закончит 
// инициализацию и готов к созданию окон браузера.
// Некоторые API могут использоваться только после возникновения этого события.
app.on('ready', createWindow)

// Выходим, когда все окна будут закрыты.
app.on('window-all-closed', () => {
  // Для приложений и строки меню в macOS является обычным делом оставаться
  // активными до тех пор, пока пользователь не выйдет окончательно используя Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
   // На MacOS обычно пересоздают окно в приложении,
   // после того, как на иконку в доке нажали и других открытых окон нету.
  if (win === null) {
    createWindow()
  }
})