/*!

 =========================================================
 * Vue Light Bootstrap Dashboard - v2.0.0 (Bootstrap 4)
 =========================================================

 * Product Page: http://www.creative-tim.com/product/light-bootstrap-dashboard
 * Copyright 2019 Creative Tim (http://www.creative-tim.com)
 * Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard/blob/master/LICENSE.md)

 =========================================================

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 */
import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import io from 'socket.io-client'

// LightBootstrap plugin
import LightBootstrap from './light-bootstrap-main'

// router setup
import routes from './routes/routes'
import VueNotify from 'vue-notifyjs'

import 'vue-notifyjs/themes/default.css'
import './registerServiceWorker'
import config from './config'
import moment from 'moment'
console.log(process.env)


// plugin setup
Vue.use(VueRouter)
Vue.use(LightBootstrap)
Vue.use(VueNotify)

io.connectAsync = async function(url, options) {
  return new Promise(function(resolve, reject) {
      io.connect(url, options);
      io.once('connect', function(socket) {
        console.log(socket)
        resolve(socket);
      });
      io.once('connect_error', function() {
        reject(new Error('connect_error'));
      });
      io.once('connect_timeout', function() {
        reject(new Error('connect_timeout'));
      });
  });
}

const socket = async () => {
  const url = config.apiServerURL;
  console.log('SERVER URL', url)
  const socket = io.connect(url);


  socket.on('error', function (reason) {
    console.error(reason)
    app.notify('Socket Error', reason)
  })
  socket.on('connect_error', (err) => {
    console.error(err)
    app.notify('Socket Error', err)
  });
 
  console.log('socket ready', socket)
  return socket
}


const boot = async () => {
  // configure router
  const router = new VueRouter({
    routes, // short for routes: routes
    linkActiveClass: 'nav-item active',
    scrollBehavior: (to) => {
      if (to.hash) {
        return {selector: to.hash}
      } else {
        return { x: 0, y: 0 }
      }
    },
    props: true
  })

  const _io = await socket()
  
  router.io = _io;
  router.state = {};

  router.beforeEach((to, from, next) => {
  
    if(to.matched.some(record => record.meta.requiresAuth)) {
      let user = JSON.parse(localStorage.getItem('user'));
      let notAuthed = (!localStorage.getItem('isAuthed') || !user|| !user.access_token) 
      let authedCheck = (localStorage.getItem('isAuthed') && user && user.access_token)
      if(user && authedCheck)
      {
        router.io.emit('action', { method: 'signin',  login: user.email, token: user.access_token, ref_token: user.refresh_token,syncTime: moment().parseZone().format() }, (authresult) => {
          console.log(authresult)
          if(authresult[0] == 'signin-ok')
          {
            localStorage.setItem('user', JSON.stringify(authresult[1]))
            next()
          }
          else
          {
            console.error('AUTH ERR RESULT: 1: ', authresult[1])
            next({
              path: '/signin',
              params: { nextUrl: to.fullPath} 
            })
          }
        })
      }
      else if(notAuthed && !authedCheck)
      {
        next({
          path: '/signin',
          params: { nextUrl: to.fullPath}
        })
      }
      else
        next()
    }
    else
    {
      next()
    }
  });

  /* eslint-disable no-new */
const app =  new Vue({
    el: '#app',
    render: h => h(App),
    router,
    methods: {
      notify (title, body, type) {
        this.$notify({
          title,
          message: body,
          type: type || 'warning'
        })
      }
    },
    mounted(){
      this.$router.io.on('loggedout', () => {
        localStorage.removeItem('isAuthed');
        localStorage.removeItem('user');
        this.$router.push('/signin');
      })
      this.$router.io.on("signin-ok", (user) => {
        localStorage.setItem('isAuthed', true)
        localStorage.setItem('user', JSON.stringify(user))
        this.$router.push("/dashboard/overview")
      })
      this.$router.io.on("user", (user) => {
        localStorage.setItem('isAuthed', true)
        localStorage.setItem('user', JSON.stringify(user))
      })
      this.$router.io.on("signin-fail", (reason) => {
        console.error(reason)
        this.notify('Error', reason, 'danger')
      })
      this.$router.io.on("signup-fail", (reason) => {
        console.error(reason)
        this.notify('Error', reason, 'danger')
      })
    }
  })
}


boot();