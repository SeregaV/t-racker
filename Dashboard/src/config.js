'use strict';

import dotenv from 'dotenv'
import fs from 'fs'
import path from 'path'

console.log(fs, dotenv)

const env = dotenv.config().parsed
// console.log(env, __dirname, __filename, process.cwd(), path.resolve(__dirname))
// process.env = { 
//     ...process.env, 
//     ...env
// }

export default {
    apiServerURL: process.env.NODE_ENV === 'development' ? 'http://localhost' : 'https://t-racker.herokuapp.com'
};